import json

class Conductor(object):

	######
	# Método de inicialización. Este valida que el objeto JSON cumpla las condiciones mínimas 
	######
	def __init__(self, conductor_json):
		# self.nombre = nombre
		# self.cedula = cedula
		self.__dict__ = json.loads(conductor_json)
		if not hasattr(self, 'nombre'):
			raise Exception("El objeto no posee un nombre, no cumple los requisitos mínimos");
		if not hasattr(self, 'cedula'):
			raise Exception("El objeto no posee una cédula, no cumple los requisitos mínimos");
	
	######
	# Método para adquirir la información adicional en texto plano
	######
	def get_aditional_info(self):
		attributes = dir(self)
		info = ""
		for attribute in attributes:
			if "_" not in attribute and attribute not in ["nombre", "cedula"]:
				info += attribute + ": " + str(getattr(self, attribute)) + "\n"
		return info