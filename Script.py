import Carro
import Conductor
import time

exit = False

while not exit:
	######
	# Se adquiere la información necesaria para deserializar el objeto
	######
	option = input("-----DESERIALIZADOR DE OBJETOS EN JSON-----\n" +
				   "Escriba el número de la opción que requiera\n" + 
				   "1. Deserializar un objeto carro\n" + 
				   "2. Deserializar un objeto conductor\n" +
				   "Puede salir presionando cualquier otra tecla\n")
	
	######
	# Si se escoge deserializar un carro, se pide el objeto serializado y se transforma en objeto python para 
	# mostrar la información pertinente
	######
	if option == "1":
		car = input("Escriba el JSON con, por lo menos, los atributos presentados en el ejemplo a continuación\n" + 
					"{ \"marca\": \"BMW\", \"modelo\": 1994 }\n")
		try:
			car_object = Carro.Carro(car)
			print("\nEl carro es de marca " + car_object.marca + " y es un modelo del año " + str(car_object.modelo))
			print("Información adicional del carro se presenta a continuación:\n" + car_object.get_aditional_info())
		
		except Exception as err:
			print("\n\nERROR: " + str(err) + "\n\n")
	
	######
	# Si se escoge deserializar un conductor, se pide el objeto serializado y se transforma en objeto python para 
	# mostrar la información pertinente
	######
	elif option == "2":
		driver = input("Escriba el JSON con, por lo menos, los atributos presentados en el ejemplo a continuación\n" + 
					   "{ \"nombre\":\"Carlos Duarte\", \"cedula\": 1234567 }\n")
		try:
			driver_object = Conductor.Conductor(driver)
			print("\nEl conductor se llama " + driver_object.nombre + ", identificado con la cédula " + str(driver_object.cedula))
			print("Información adicional del conductor se presenta a continuación:\n" + driver_object.get_aditional_info())
		
		except Exception as err:
			print("\n\nERROR: " + str(err) + "\n\n")
	
	######
	# En cualquier otro caso, la aplicación termina 
	######
	else: 
		print("Saliendo...")
		exit = True
	
	######
	# Si no se específica una salida explícitamente, la aplicación continúa después de dos segundos
	######
	if not exit:
		print("Continuando...")
		time.sleep(2)

	
	
	