import json

class Carro(object): 
	
	######
	# Método de inicialización. Este valida que el objeto JSON cumpla las condiciones mínimas 
	######
	def __init__(self, carro_json):
		# self.marca = marca
		# self.modelo = modelo
		self.__dict__ = json.loads(carro_json)
		if not hasattr(self, 'marca'):
			raise Exception("El objeto no posee una marca, no cumple los requisitos mínimos"); # prueba merge Juan D.
		if not hasattr(self, 'modelo'):
			raise Exception("El objeto no posee un modelo, no cumple los requisitos mínimos");
	
	######
	# Método para adquirir la información adicional en texto plano
	######
	def get_aditional_info(self):
		attributes = dir(self)
		info = ""
		for attribute in attributes:
			if "_" not in attribute and attribute not in ["marca", "modelo"]:
				info += attribute + ": " + str(getattr(self, attribute)) + "\n"
		return info